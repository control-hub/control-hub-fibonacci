using control_hub_fibonacci_support.Extensions;
using control_hub_fibonacci_support.Managed;
using control_hub_fibonacci_support.Support.Managed;
using control_hub_fibonacci_ws.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace control_hub_fibonacci_ws
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options => options.Filters.Add(new HttpResponseExceptionFilter()));
            ControlarCorsOrigin(services, Configuration);
            Ioc.AddDependency(services);
            //services.AddAuthorization();
            ControlarExcepcionPeticion(services);
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "control-hub-fibonacci-ws API V1", Version = "1" });
                //c.DocInclusionPredicate((docName, apiDesc) => GetPredicate(docName, apiDesc));
                //c.OperationFilter<RemoveVersionParameterFilter>();
                //c.DocumentFilter<SetVersionInPathFilter>();
            });
            services.AddApiVersioning(o =>
            {
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                o.ReportApiVersions = true;
                o.ApiVersionReader = ApiVersionReader.Combine(
                    new QueryStringApiVersionReader("api-version"),
                    new HeaderApiVersionReader("X-Version"),
                    new MediaTypeApiVersionReader("ver"));
               
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IConfiguration configuration)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "control-hub-fibonacci-ws API V1");
                    c.DefaultModelsExpandDepth(-1);
                });
            }

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseHttpsRedirection();
            app.UseCors("MyPolicy");
            app.UseRouting();

            //app.UseAuthorization();

            SetupSerilog(configuration);

            app.UseSerilogRequestLogging();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        public bool GetPredicate(string docName, ApiDescription apiDescription)
        {
            var actionApiVersionModel = apiDescription.ActionDescriptor?.GetApiVersion();
            if (actionApiVersionModel == null)
            {
                return true;
            }
            if (actionApiVersionModel.DeclaredApiVersions.Any())
            {
                return actionApiVersionModel.DeclaredApiVersions.Any(version => $"v{version.ToString()}".Equals(docName));
            }
            return actionApiVersionModel.ImplementedApiVersions.Any(version => $"v{version.ToString()}".Equals(docName));
        }

        private static void ControlarCorsOrigin(IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                  "MyPolicy",
                   builder => builder.AllowAnyOrigin()
                  .AllowAnyMethod()
                  .AllowAnyHeader()
                  //.AllowCredentials()
                  );
            });
        }

        private static void ControlarExcepcionPeticion(IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    string messages = "";
                    if (!context.ModelState.IsValid)
                    {
                        messages = string.Join("; ", context.ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));
                    }
                    throw new ArgumentNullException(messages);
                };
            });
        }
        private void SetupSerilog(IConfiguration configurationApp)
        {
            var path = configurationApp["Serilog:PathLog"];
            var pathErrorFinal = Path.Combine(path + "control-hub-log-error.txt");
            var pathDebuFinal = Path.Combine(path + "control-hub-log-debug.txt");
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();
            Log.Logger = new LoggerConfiguration().ReadFrom
                .Configuration(configuration)
                .Enrich.FromLogContext()
                .WriteTo.Console(LogEventLevel.Information)
              .WriteTo.RollingFile(pathErrorFinal, LogEventLevel.Warning, retainedFileCountLimit: 15)
              .WriteTo.RollingFile(pathDebuFinal, LogEventLevel.Debug, retainedFileCountLimit: 1)
              .CreateLogger();
            Log.Debug("Successfully setup Serilog");
        }
    }
}
