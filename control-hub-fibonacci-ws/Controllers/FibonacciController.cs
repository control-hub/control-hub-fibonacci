﻿using control_hub_fibonacci_service.Service;
using Microsoft.AspNetCore.Mvc;
using osiptel_sicalt_support.Support.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace control_hub_fibonacci_ws.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class FibonacciController : Controller
    {
        private readonly IFibonacciService fibonacciService;

        public FibonacciController(IFibonacciService fibonacciService)
        {
            this.fibonacciService = fibonacciService;
        }
        [HttpGet("element")]
        public async Task<Response<decimal>> Authentication(int indexSeach)
        {
            var user = await fibonacciService.GetElement(indexSeach);
            var response = new Response<decimal>();
            return response.Ok(user);
        }
    }
}
