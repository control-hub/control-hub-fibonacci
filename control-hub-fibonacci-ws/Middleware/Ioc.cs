﻿using control_hub_fibonacci_service.Service;
using control_hub_fibonacci_service.Service.Impl;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace control_hub_fibonacci_ws.Middleware
{
    public static class Ioc
    {
        public static IServiceCollection AddDependency(this IServiceCollection services)
        {
            services.AddSingleton(Log.Logger);
            services.AddTransient<IFibonacciService, FibonacciService>();
            return services;
        }
    }
}
