﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace control_hub_fibonacci_service.Service.Impl
{
    public class FibonacciService : IFibonacciService
    {
        public async Task<decimal> GetElement(int indexSeach)
        {
            var listSerie = new List<decimal> { { 0 }, { 1 } };
            var indiceCalculate = 2;
            while (indiceCalculate <= indexSeach)
            {
                var listLast = listSerie.GetRange(listSerie.Count - 2, 2);
                var sumaLast = listLast.Sum();
                listSerie.Add(sumaLast);
                indiceCalculate++;
            }
            return listSerie[indexSeach];
        }
    }
}
