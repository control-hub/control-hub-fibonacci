﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace control_hub_fibonacci_service.Service
{
    public interface IFibonacciService
    {
        Task<decimal> GetElement(int indexSeach);
    }
}
