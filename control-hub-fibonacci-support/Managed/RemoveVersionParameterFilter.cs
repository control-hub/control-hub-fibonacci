﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
namespace control_hub_fibonacci_support.Managed
{
    public class RemoveVersionParameterFilter : IOperationFilter
    {
        void IOperationFilter.Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var parameterVersion = operation?.Parameters?.SingleOrDefault(param => param.Name.Equals("version"));
            operation?.Parameters?.Remove(parameterVersion);
        }
    }
}
