﻿namespace control_hub_fibonacci_support.Support.Managed
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class HttpResponseException : Exception
    {
        private HttpResponseException()
        {
        }
        public HttpResponseException(string message)
          : base(message)
        {
        }

        public HttpResponseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected HttpResponseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}