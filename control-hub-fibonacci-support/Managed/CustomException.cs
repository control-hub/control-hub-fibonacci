﻿namespace control_hub_fibonacci_support.Support.Managed
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class CustomException : Exception
    {
        public string CustomMensaje { get; set; }
        public CustomException(string message) : base(message)
        {
        }

        public CustomException(string message, string customMensaje) : base(message)
        {
            CustomMensaje = customMensaje;
        }

        public CustomException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CustomException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
