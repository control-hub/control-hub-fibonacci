﻿namespace control_hub_fibonacci_support.Support.Managed
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using osiptel_sicalt_support.Support.Constant;
    using osiptel_sicalt_support.Support.Dto;
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    public class ErrorHandlingMiddleware
    {
        private readonly ILogger<ErrorHandlingMiddleware> _logger;
        private readonly RequestDelegate _next;
        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger)
        {
            this._next = next;
            this._logger = logger;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            _logger.LogInformation($":::::::::::::::::::::::::::::::::: START ${context.Request.Path} :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
            try
            {
                var request = context.Request;
                if (request?.Body?.CanRead == true)
                {
                    request.EnableBuffering();
                    var bodySize = (int)(request.ContentLength ?? request.Body.Length);
                    if (bodySize > 0)
                    {
                        request.Body.Position = 0;
                        byte[] body;
                        using (var ms = new MemoryStream(bodySize))
                        {
                            await request.Body.CopyToAsync(ms);
                            body = ms.ToArray();
                        }
                        request.Body.Position = 0;
                        var requestBodyString = Encoding.UTF8.GetString(body);
                        _logger.LogInformation(requestBodyString);
                    }
                }
                await _next(context);
            }
            catch (System.Exception ex)
            {
                await HandleExceptionAsync(context, ex, _logger);
            }
            _logger.LogInformation($":::::::::::::::::::::::::::::::::: END ${context.Request.Path} :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

        }

        private static Task HandleExceptionAsync(HttpContext context, System.Exception ex, ILogger<ErrorHandlingMiddleware> logger)
        {
            var response = new Response();
            String message;
            if (ex is CustomException)
            {
                message = ex.Message;
                logger.LogWarning(ex, "HandleException");
            }
            else
            {
                message = Message.algoSalioMal;
                logger.LogError(ex, "NotHandleException");
            }
            response.Message = message;
            response.MessageDev = ex.Message + " " + ex?.InnerException?.Message;

            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var result = JsonConvert.SerializeObject(response, jsonSerializerSettings);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return context.Response.WriteAsync(result);
        }
    }
}
