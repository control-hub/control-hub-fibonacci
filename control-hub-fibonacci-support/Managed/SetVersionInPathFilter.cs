﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace control_hub_fibonacci_support.Managed
{
    public class SetVersionInPathFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Paths = (OpenApiPaths)swaggerDoc.Paths
               .ToDictionary(
                   path => path.Key.Replace("{version}", swaggerDoc.Info.Version),
                   path => path.Value
               );
        }
    }
}
