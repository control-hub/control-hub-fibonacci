﻿
namespace osiptel_sicalt_support.SupportDto
{
    public class SeleccionOpcion
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Code { get; set; }
        public int? IdCondition { get; set; }
    }
}
