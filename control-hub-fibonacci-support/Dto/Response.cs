﻿
using osiptel_sicalt_support.Support.Constant;

namespace osiptel_sicalt_support.Support.Dto
{
    public class Response<T>
    {
        public string Message { get; set; }
        public T Result { get; set; }
        public string MessageDev { get; set; }
        public Response<T> Ok(T dato)
        {
            this.Message = Constant.Message.correcto;
            this.Result = dato;
            return this;
        }
        public Response<T> Ok(T dato, string mensage)
        {
            this.Message = mensage;
            this.Result = dato;
            return this;
        }
    }

    public class Response
    {
        public string Message { get; set; }
        public string MessageDev { get; set; }
        public Response Ok()
        {
            this.Message = Constant.Message.correcto;
            return this;
        }
    }
}
