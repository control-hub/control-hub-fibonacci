﻿namespace osiptel_sicalt_support.Support.Constant
{
    public static class Message
    {
        public readonly static string correcto = "Operación Completada";
        public readonly static string listadoCorrecto = "Operacion Completada, datos listado correctamente";
        public readonly static string inactivarCorrecta = "Operacion Completada, registro eliminado correctamente";
        public readonly static string actualizacionCorrecta = "Operación Completada, registro actualizado correctamente";
        public readonly static string sinRegistros = "No se han encontrado registros";
        public readonly static string error = "Se ha presentado un error";
        public readonly static string algoSalioMal = "Algo salio mal";
        public readonly static string datosIncompletos = "Datos imcompletos para procesar su información";
        public readonly static string respuestaLogin = "Bienvenido";
    }
}
